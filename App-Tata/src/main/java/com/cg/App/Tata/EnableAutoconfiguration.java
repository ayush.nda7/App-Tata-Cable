package com.cg.App.Tata;

import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

public @interface EnableAutoconfiguration {

	Class<DataSourceAutoConfiguration>[] exclude();

}
