package com.cg.App.Tata.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cg.App.Tata.Module.Pack;

@Repository
public interface IPackRepository extends JpaRepository<Pack,Long>{

	Pack add(Pack pack);

	List<Pack> findPacksGreaterThanAmount(double amount);

	List<Pack> findPacksInAscendingOrderByDaysValidity();

	List<Pack> findPacksInAscendingOrderByCost();

	List<Pack> popularPacks();
	
 
}