package com.cg.App.Tata.Service;

import java.util.Optional;

import org.hibernate.sql.Update;
import org.springframework.stereotype.Service;

import com.cg.App.Tata.Module.User;
@Service
public class IUserServiceImpl implements IUserService {
	@Autowired
	IUserRepository iuserrepository;
	@Override
	public User register(User user) {
		// TODO Auto-generated method stub
		
		return iuserrepository.save(user);
	}

	@Override
	public User update(User user) {
		// TODO Auto-generated method stub
		Optional<User> updateuser =iuserrepository.findById(user.getId());
		if(updateuser.isPresent()) {
			iuserrepository.saveAndFlush(user);
			
		}
		return user;
	}

	public User findById(Long id) {
		// TODO Auto-generated method stub
		Optional<User> userbyid= iuserrepository.findById(id);
		return userbyid.get();
	}

	@Override
	public User findByUsername(String username) {
		// TODO Auto-generated method stub
		Optional<User> userbyname= iuserrepository.findAll(username);
		return userbyname.get();
		
	}

	@Override
	public String deleteByUserId(Long userId) {
		iuserrepository.deleteAll(userId);
		return "Record deleted";
		// TODO Auto-generated method stub
		
	}

}