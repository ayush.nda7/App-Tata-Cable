package com.cg.App.Tata.Service;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.App.Tata.Module.Pack;
import com.cg.App.Tata.Repository.IPackRepository;
@Service
public class IPackServiceImpl implements IPackService{
@Autowired
IPackRepository packRepository;
	@Override
	public Pack add(Pack pack) {
		return packRepository.save(pack);
	}

	@Override
	public Pack update(Pack pack) {
		return packRepository.save(pack);
	}
	
	@Override
	public Pack findPackById(Long packId) {
		 Optional<Pack> opt = packRepository.findById(packId);
		Pack pck = opt.get();
		return pck;
		}

	@Override
	public List<Pack> findPacksGreaterThanAmount(double amount) {
		List<Pack> list = packRepository.findPacksGreaterThanAmount(amount);
		return list;
	}

	@Override
	public List<Pack> findPacksInAscendingOrderByDaysValidity() {
		List<Pack> list = packRepository.findPacksInAscendingOrderByDaysValidity();
		return list;
	}

	@Override
	public List<Pack> findPacksInAscendingOrderByCost() {
		// TODO Auto-generated method stub
		List<Pack> list = packRepository.findPacksInAscendingOrderByCost();
	    return list;
	}

	@Override
	public List<Pack> popularPacks() {
		// TODO Auto-generated method stub
		List<Pack> list = packRepository.popularPacks();
		return list;
	}

	@Override
	public String deleteByPackId(Long packId) {
		return null;
		
	}

}