package com.cg.App.Tata.Ctrl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cg.App.Tata.Module.Account;
import com.cg.App.Tata.Module.Pack;
import com.cg.App.Tata.Module.Recharge;
import com.cg.App.Tata.Service.IRechargeService;

@RestController
@RequestMapping("/api/Recharge")
public class RechargeController {
	@Autowired
	IRechargeService rcgservice;
	
	@PostMapping("/createRecharge")
	public Recharge createRecharge(@RequestBody Pack pack, Account account) {
		return rcgservice.createRecharge(pack, account);
	}

	@PostMapping("/updateRecharge")
	public Recharge update(@RequestBody Recharge recharge) {
		return rcgservice.update(recharge);
	}

	@GetMapping("/finRechargesbyPurchaseDate")
	public List<Recharge> findRechargesForUserInDescendingOrderByPurchasedDate(@RequestBody Account acccount) {
		return rcgservice.findRechargesForUserInDescendingOrderByPurchasedDate(acccount);
	}

	@GetMapping("/rechargesforUserCount")
	public int rechargesForUserCount(@RequestBody Account account) {
		return rcgservice.rechargesForUserCount(account);
	}

	@GetMapping("/findRecharges")
	public List<Recharge> findAllRechargesInPeriod(@RequestBody LocalDate startDate, LocalDate endDate) {
		return rcgservice.findAllRechargesInPeriod(startDate, endDate);
	}

	@GetMapping("/countRecharges")
	public int countRechargesInPeriod(@RequestBody LocalDate startDate, LocalDate endDate) {
		return rcgservice.countRechargesInPeriod(startDate, endDate);
	}

	@GetMapping("/totalrevenueinPeriod")
	public double totalRevenueInPeriod(@RequestBody LocalDate startDate, LocalDate endDate) {
		return rcgservice.totalRevenueInPeriod(startDate, endDate);
	}

	@GetMapping("/rechargesCount}")
	public int rechargesCount(@RequestBody Pack pack) {
		return rcgservice.rechargesCount(pack);
	}

	@PostMapping("/expireifValidityFinished")
	public Recharge expireIfValidityFinished(@RequestBody Account account, Recharge recharge) {
		return rcgservice.expireIfValidityFinished(account, recharge);
	}

}
