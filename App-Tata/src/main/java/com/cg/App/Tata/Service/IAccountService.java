package com.cg.App.Tata.Service;

import java.time.LocalDate;
import java.util.List;

import com.cg.App.Tata.Module.Account;
import com.cg.App.Tata.Module.Pack;



public interface IAccountService {
  
	Account findById(Long accountId);
	Account updateAccount(Account account);
	String deleteByAccountId(Long accountId);
	List<Account> getCreatedAccounts();
	List<Account> getCreatedAccountsInPeriod(LocalDate startDate, LocalDate endDate);
	int countAccounts();
	String removePackForAccount(Account account, Pack pack);
	Account createAccount(Account account);
}
