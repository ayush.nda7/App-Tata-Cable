package com.cg.App.Tata.Ctrl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cg.App.Tata.Module.Account;
import com.cg.App.Tata.Module.Pack;
import com.cg.App.Tata.Service.IAccountService;

@RestController
@RequestMapping("/api")
public class AccountController {
	@Autowired
	IAccountService accservice;
	
	@GetMapping("/findbyId/{id}")
	public Account findById(@RequestBody Long accountId) {
		
		return accservice.findById(accountId);
		
	}

	@PostMapping("/accounts")
	public Account createAccount(@RequestBody Account account) {
		
		return accservice.createAccount(account);
	}
	@PutMapping("/accounts")
	public Account updateAccount(@RequestBody Account account) {
		
		return accservice.updateAccount(account);
	}

	@DeleteMapping("/deleteById/{accountId}")
	public String deleteByAccountId(@PathVariable Long accountId) {
		
		return accservice.deleteByAccountId(accountId);
	}

	@GetMapping("/accountscreated")
	public List<Account> getCreatedAccounts() {
		
		return accservice.getCreatedAccounts();
	}

	@GetMapping("/accountscreatedinperiod/{startDate}/{endDate}")
	public List<Account>  getCreatedAccountsInPeriod(@PathVariable LocalDate startDate, @PathVariable LocalDate endDate) {
		
		return accservice.getCreatedAccountsInPeriod(startDate, endDate);
		
	}

	@GetMapping("/totalAccounts")
	public int countAccounts() {
		
		return accservice.countAccounts();
	}

	@DeleteMapping("/removepackaccount")
	public String removePackForAccount(@RequestBody Account account, Pack pack) {
		
		return accservice.removePackForAccount(account, pack);
		
	}

	
	
}
