package com.cg.App.Tata.Service;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.App.Tata.Module.Account;
import com.cg.App.Tata.Module.ServiceRequest;
import com.cg.App.Tata.Repository.IServiceRequestRepository;


@Service
public class IServiceRequestServiceImpl implements IServiceRequestService {
	
	@Autowired
	private IServiceRequestRepository sdao;

	@Override
	public ServiceRequest createServiceRequestForUser(Account account) {
		ServiceRequest csr = new ServiceRequest(account,"Service Request is created ");
		return sdao.save(csr);
	}

	@Override
	public ServiceRequest openedServiceRequest(Account account) {
		ServiceRequest osr = sdao.findOpenedServiceRequest(account);
		return sdao.save(osr);
	}

	@Override
	public ServiceRequest close(Long serviceRequestId) {
		
		Optional<ServiceRequest> opt = sdao.findById(serviceRequestId);
		
		ServiceRequest req = opt.get();
		req.setStatusOpened(false);
	
		sdao.save(req);
		return req;
	}

}