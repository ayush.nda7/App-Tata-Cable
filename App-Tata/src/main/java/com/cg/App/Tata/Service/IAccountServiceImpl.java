package com.cg.App.Tata.Service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.App.Tata.Module.Account;
import com.cg.App.Tata.Module.Pack;
import com.cg.App.Tata.Repository.IAccountRepository;

@Service
public class IAccountServiceImpl implements IAccountService {
@Autowired
IAccountRepository accountRepository;
	@Override
	public Account findById(Long accountId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Account createAccount(Account account) {
		return accountRepository.save(account);
	}

	@Override
	public String deleteByAccountId(Long accountId) {
		return null;
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Account> getCreatedAccounts() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Account> getCreatedAccountsInPeriod(LocalDate startDate, LocalDate endDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int countAccounts() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String removePackForAccount(Account account, Pack pack) {
		return null;
		// TODO Auto-generated method stub
		
	}

	@Override
	public Account updateAccount(Account account) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
