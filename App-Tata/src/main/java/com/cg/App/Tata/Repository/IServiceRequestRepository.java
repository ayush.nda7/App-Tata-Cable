package com.cg.App.Tata.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cg.App.Tata.Module.Account;
import com.cg.App.Tata.Module.ServiceRequest;

@Repository
public interface IServiceRequestRepository extends JpaRepository<ServiceRequest,Long> {

	@Query("FROM ServiceRequest WHERE statusOpened = 'true' and account =:account")
	ServiceRequest findOpenedServiceRequest(@Param("account") Account account);
	
	@Query("FROM ServiceRequest WHERE statusOpened = 'true' and accountId =:accountId")
	ServiceRequest findOpenedServiceRequest(@Param("accountId") Long accountId);
	
}